@extends('layout')

@section('titulo')

@stop

@section('conteudo')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
		<h1>Edite a anotação</h1>
			<form method="POST" action="/notes/{{ $note->id }}">
			{{method_field('PATCH')}}

	 		<div class="form-group">
	 		<textarea name="body" class="form-control">{{$note->body}}</textarea>
	 		</div>
	 		<div class="form-group">
	 		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	 		<button type="submit" class="btn btn-primary">Atualizar nota</button>
	 		</div>

	 	</form>
		</div>
	</div>
@stop