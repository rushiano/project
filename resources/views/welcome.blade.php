@extends('layout')
@section('head')
<link rel="stylesheet" href="/css/app.css">
@stop
@section('topo')
    @include('flash')
              <div class="top-right links">
                <a href="{{ url('about') }}">Sobre</a>
                </div>
@stop
@section('conteudo')
    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>
        <div>
            <div><h1><a href="/cards">Ver cards</a></h1></div>
            
            @unless (empty($people))
                Nomes disponiveis:
                @foreach ($people as $person)
                    <li> {{ $person }}</li>
                @endforeach
            @else
                Welcome
            @endunless
        </div>
    </div> 
@stop   