<?php

/*Route::get('/', function () {
    $people = ['Taylor','Luciano', 'Jefrey'];
    return view('welcome', compact('people'));
    //return view('welcome');
    //return 'Olá mundo!';
});

Route::get('about', function(){
	return view('pages.about'); //resources/views/about.blade.php
});
*/
Route::group(['middleware' => ['web']], function(){

	class Mailer{

	}

	class RegistersUsers{

		protected $mailer;

		public function __construct(Mailer $mailer){
			$this->mailer = $mailer;

		}
		public function setMailer(Mailer $mailer){
			$this->mailer = $mailer;

		}
	}

	App::bind('foo', function(){
		return new RegistersUsers(new Mailer);
	});

	$one = app('foo');
	$two = app('foo');
	//var_dump(App::make('foo'));
	var_dump($one, $two);

	Route::get('begin',function(){
		//Session::flash('status','Hello there');
		flash('Você está logado','success');
		
		return redirect('/');
	});

	
	Route::get('/', 'PagesController@home');
	Route::get('about', 'PagesController@about');

	Route::get('cards', 'CardsController@index');
	Route::get('cards/{card}', 'CardsController@show');

	Route::post('cards/{card}/notes','NotesController@store');
	Route::get('notes/{note}/edit','NotesController@edit');
	Route::patch('notes/{note}','NotesController@update');
});


